package com.example.pratik.yumm;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.util.Log;
import android.widget.TextView;

/**
 * Created by Pratik Karanje on 9/5/16.
 */
public class CustomTextView extends android.support.v7.widget.AppCompatTextView {

    public static final String TAG = CustomTextView.class.getSimpleName();

    public CustomTextView(Context context) {
        super(context);
    }

    public CustomTextView(Context context, AttributeSet attrs) {
        super(context, attrs);
        setCustomTypeface(context, attrs, 1);
    }

    private void setCustomTypeface(Context context, AttributeSet attrs, int defStyle) {
        TypedArray a = context.obtainStyledAttributes(attrs, R.styleable.hunger, defStyle, 0);
        String fontName = a.getString(R.styleable.hunger_typeface);
        if (fontName != null) {
            try {
                Typeface typeface = Typeface.createFromAsset(context.getAssets(), "fonts/" + fontName);
                setTypeface(typeface);
            } catch(Exception e) {
                Log.e(TAG, "" + e);
            }
        }
        a.recycle();
    }
}
