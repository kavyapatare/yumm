package com.example.pratik.yumm;

import android.content.Context;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.List;

import static android.R.attr.id;

/**
 * Created by kaveri on 31/8/17.
 */

public class MyBookingsListAdapter extends RecyclerView.Adapter<MyBookingsListAdapter.ViewHolder> {

    Context context;
    private List<BookingDetails> myBookingsList;

    public abstract class ViewHolder extends RecyclerView.ViewHolder{


        public ViewHolder(View itemView) {

            super(itemView);

        }
        public abstract void bindViewHolder(int position);
    }


    public class MyViewHolder extends ViewHolder {


        ImageView image1,image2,image3,imageGo;
        TextView restaurantName,bookingType,day,time,personCount,orderName;


        public MyViewHolder(View view) {
            super(view);
            image1 = (ImageView) view.findViewById(R.id.img_dot1);
            image2 = (ImageView) view.findViewById(R.id.img_dot2);
            image3 = (ImageView) view.findViewById(R.id.img_dot3);
            imageGo = (ImageView) view.findViewById(R.id.img_go);

            restaurantName=(TextView)view.findViewById(R.id.txt_restaurantName);
            bookingType=(TextView)view.findViewById(R.id.txt_bookingType);
            day=(TextView)view.findViewById(R.id.txt_date);
            time=(TextView)view.findViewById(R.id.txt_time);
            personCount=(TextView)view.findViewById(R.id.txt_persons);
            orderName=(TextView)view.findViewById(R.id.txt_orderName);
        }

        @Override
        public void bindViewHolder(int position)
        {
            BookingDetails details= (BookingDetails) myBookingsList.get(position);
            restaurantName.setText(details.getResturantName());
            if(details.getBookingStatus()==BookingDetails.STATUS_CONFIRMED) {
                bookingType.setTextColor(ContextCompat.getColorStateList(context, R.color.green));
                bookingType.setText("Confirmed");
            }
            if(details.getBookingStatus()==BookingDetails.STATUS_CANCELLED) {
                bookingType.setTextColor(ContextCompat.getColorStateList(context, R.color.light_red));
                bookingType.setText("Cancelled");
            }
            if(details.getBookingStatus()==BookingDetails.STATUS_REQUESTED) {
             //   bookingType.setTextColor(ContextCompat.getColorStateList(getContext(), R.color.));
                bookingType.setText("Requested");
            }
            if(details.getBookingStatus()==BookingDetails.STATUS_ELAPSED) {
                //   bookingType.setTextColor(ContextCompat.getColorStateList(getContext(), R.color.));
                bookingType.setText("Elapsed");
            }
            day.setText(details.getDate());
            time.setText(details.getTime());
            personCount.setText(details.getPersonCount());
            orderName.setText(details.getOrderName());
        }


    }



    public MyBookingsListAdapter(List<BookingDetails> myBookingsList,Context c) {

        this.myBookingsList = myBookingsList;
        this.context=c;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType)
    {

            View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.bookinglist_item_view, parent, false);
            return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        holder.bindViewHolder(position);
    }

    @Override
    public int getItemCount() {
        return myBookingsList.size();
    }

}
