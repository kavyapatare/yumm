package com.example.pratik.yumm;

import android.view.View;
import android.widget.AdapterView;

/**
 * Created by kaveri on 30/8/17.
 */

public class SpinnerHandler implements AdapterView.OnItemSelectedListener {
    private boolean isFirst = true;

    public SpinnerHandler(BookingActivity bookingActivity) {

    }

    @Override
    public void onItemSelected(AdapterView<?> spinner, View view, int selectedIndex, long l) {
        if (isFirst) {
            isFirst = false;
        } else {
            spinner.setSelection(selectedIndex);
        }
    }

    @Override
    public void onNothingSelected(AdapterView<?> spinner) {
        spinner.setSelection(0);

    }
}
