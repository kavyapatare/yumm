package com.example.pratik.yumm;

import android.media.Image;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.List;

import static android.R.attr.id;

/**
 * Created by kaveri on 29/8/17.
 */

public class RestaurantFilterAdapter extends RecyclerView.Adapter<RestaurantFilterAdapter.ViewHolder> {


    private List<FilterRowType> restaurantFilterList;

    public abstract class ViewHolder extends RecyclerView.ViewHolder{


        public ViewHolder(View itemView) {

            super(itemView);
        }
        public abstract void bindViewHolder(int position);
    }

    public class HeaderViewHolder extends ViewHolder {
        public LinearLayout header;
        public TextView headerText;
        public HeaderViewHolder(View itemView) {
            super(itemView);
            header = (LinearLayout) itemView.findViewById(R.id.headerView);
            headerText=(TextView)itemView.findViewById(R.id.userName);
        }

        @Override
        public void bindViewHolder(int position) {
            FilterHeader filterHeader= (FilterHeader) restaurantFilterList.get(position);
          headerText.setText("Hola " +filterHeader.getHeader());

        }
    }

    public class MyViewHolder extends ViewHolder {
        ImageView image;

        public MyViewHolder(View view) {
            super(view);
            image = (ImageView) view.findViewById(R.id.img_filterCategory);
        }

        @Override
        public void bindViewHolder(int position) {
            FilterDetails details= (FilterDetails) restaurantFilterList.get(position);
            image.setBackgroundResource(details.getImageId());
        }

    }



    public RestaurantFilterAdapter(List<FilterRowType> restaurantFilterList) {

        this.restaurantFilterList = restaurantFilterList;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        if (viewType == FilterRowType.HEADER_TYPE) {
            View layoutView = LayoutInflater.from(parent.getContext()).inflate(R.layout.header_view, parent, false);
            return new HeaderViewHolder(layoutView);
        } else if(viewType==FilterRowType.ITEM_TYPE)
        {
            View itemView = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.row_filter_categories, parent, false);

            return new MyViewHolder(itemView);
        }
        return null;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
     holder.bindViewHolder(position);
    }
    @Override
    public int getItemCount() {
        return restaurantFilterList.size();
    }

    @Override
    public int getItemViewType(int position) {
        return restaurantFilterList.get(position).getItemType();
    }
}
