package com.example.pratik.yumm;

/**
 * Created by kaveri on 29/8/17.
 */

public class FilterHeader implements FilterRowType {
    private String header;

    public FilterHeader(String header) {
        this.header = header;
    }

    public String getHeader() {
        return header;
    }

    public void setHeader(String header) {
        this.header = header;
    }

    @Override
    public int getItemType() {
        return FilterRowType.HEADER_TYPE;
    }
}
