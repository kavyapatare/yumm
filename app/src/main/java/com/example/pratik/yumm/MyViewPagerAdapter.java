package com.example.pratik.yumm;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.app.FragmentTransaction;

/**
 * Created by kaveri on 31/8/17.
 */

public class MyViewPagerAdapter extends FragmentPagerAdapter
{

    public MyViewPagerAdapter(FragmentManager fm)
    {
        super(fm);

    }

    @Override
    public int getCount()
    {

        return 2;
    }

    @Override
    public Fragment getItem(int position)
    {
        if(position==0)
           return new PendingBookingsFragment();
        else
            return  new PreviousBookingsFragment();
    }
}
