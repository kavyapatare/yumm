package com.example.pratik.yumm;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.util.Log;
import android.widget.EditText;

/**
 * Created by kaveri on 30/8/17.
 */

public class CustomEditText extends android.support.v7.widget.AppCompatEditText {

    public static final String TAG = CustomEditText.class.getSimpleName();

    public CustomEditText(Context context) {
        super(context);
    }

    public CustomEditText(Context context, AttributeSet attrs) {
        super(context, attrs);
        setCustomTypeface(context, attrs, 1);
    }

    private void setCustomTypeface(Context context, AttributeSet attrs, int defStyle) {
        TypedArray a = context.obtainStyledAttributes(attrs, R.styleable.hunger, defStyle, 0);
        String fontName = a.getString(R.styleable.hunger_typeface);
        if (fontName != null) {
            try {
                Typeface typeface = Typeface.createFromAsset(context.getAssets(), "fonts/" + fontName);
                setTypeface(typeface);
            } catch(Exception e) {
                Log.e(TAG, "" + e);
            }
        }
        a.recycle();
    }
}
