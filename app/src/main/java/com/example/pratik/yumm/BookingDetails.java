package com.example.pratik.yumm;

import android.media.Image;

/**
 * Created by kaveri on 31/8/17.
 */

class BookingDetails {
    public static final int STATUS_REQUESTED=0;
    public static final int STATUS_CONFIRMED=1;
    public static final int STATUS_CANCELLED=2;
    public static final int STATUS_ELAPSED=3;
    private String resturantName,bookingType,date,time,personCount,orderName;
    int bookingStatus;



    public BookingDetails(String resturantName, int bookingStatus, String date, String time, String personCount, String orderName)
    {
        this.resturantName = resturantName;
        this.bookingType = bookingType;
        this.date = date;
        this.time = time;
        this.personCount = personCount;
        this.orderName = orderName;
        this.bookingStatus=bookingStatus;


    }
    public int getBookingStatus()
    {
        return bookingStatus;
    }

    public void setBookingStatus(int bookingStatus) {
        this.bookingStatus = bookingStatus;
    }
    public String getResturantName() {
        return resturantName;
    }

    public void setResturantName(String resturantName) {
        this.resturantName = resturantName;
    }

    public String getBookingType() {
        return bookingType;
    }

    public void setBookingType(String bookingType) {
        this.bookingType = bookingType;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String getPersonCount() {
        return personCount;
    }

    public void setPersonCount(String personCount) {
        this.personCount = personCount;
    }

    public String getOrderName() {
        return orderName;
    }

    public void setOrderName(String orderName) {
        this.orderName = orderName;
    }


}
