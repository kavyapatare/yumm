package com.example.pratik.yumm;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by kaveri on 31/8/17.
 */

public class PreviousBookingsFragment extends Fragment {
    RecyclerView recyclerView;
    List<BookingDetails> myBookingsList;
    View view;
    String restaurantName="Restaurant Name";
   int bookingStatus=0;
    String date="20 June";
    String time="11:00";
    String personCount="4 persons";
    String orderName="A la carte";
    public PreviousBookingsFragment() {};

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        view=inflater.inflate(R.layout.mybookings_fragment_view,container,false);

        return view;
    }

    @Override
    public void onStart() {
        super.onStart();
        myBookingsList=new ArrayList<>();
        recyclerView=(RecyclerView)view.findViewById(R.id.bookingRecyclerView);
        myBookingsList.add(0,new BookingDetails(restaurantName,1,date,time,personCount,orderName));
        myBookingsList.add(1,new BookingDetails(restaurantName,0,date,time,personCount,orderName));
        myBookingsList.add(2,new BookingDetails(restaurantName,3,date,time,personCount,orderName));
        myBookingsList.add(3,new BookingDetails(restaurantName,2,date,time,personCount,orderName));
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        recyclerView.setHasFixedSize(true);
        recyclerView.setAdapter(new MyBookingsListAdapter(myBookingsList,getContext()));

    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
    }
}
