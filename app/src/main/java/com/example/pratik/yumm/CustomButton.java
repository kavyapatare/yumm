package com.example.pratik.yumm;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;
import android.util.Log;
import android.util.StateSet;
import android.widget.Button;



/**
 * Created by iauro003 on 9/5/16.
 */
public class CustomButton extends android.support.v7.widget.AppCompatButton {

    public static final String TAG = CustomButton.class.getSimpleName();

    private Drawable mDrawable, mSelectedDrawable;

    public CustomButton(Context context) {
        super(context);
    }

    public CustomButton(Context context, AttributeSet attrs) {
        super(context, attrs);
        setCustomTypeface(context, attrs, 0);
        mDrawable = getBackground();
    }

    public CustomButton(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    @Override
    public void setBackgroundResource(int r) {
        mDrawable = getResources().getDrawable(r);
        drawableStateChanged();
    }

    public void setSelectedResource(int r) {
        mSelectedDrawable = getResources().getDrawable(r);
    }

    @Override
    protected void drawableStateChanged() {
        int[] states = getDrawableState();
        if (StateSet.stateSetMatches(new int[]{ android.R.attr.state_pressed }, states)
                || StateSet.stateSetMatches(new int[]{ android.R.attr.state_focused }, states)) {
            setBackgroundDrawable(mSelectedDrawable);
        } else if (!StateSet.stateSetMatches(new int[]{ android.R.attr.state_enabled }, states)) {
            setBackgroundDrawable(mSelectedDrawable);
        } else {
            setBackgroundDrawable(mDrawable);
        }
        super.drawableStateChanged();
    }

    private void setCustomTypeface(Context context, AttributeSet attrs, int defStyle) {
        TypedArray a = context.obtainStyledAttributes(attrs, R.styleable.hunger, defStyle, 0);
        String fontName = a.getString(R.styleable.hunger_typeface);
        if (fontName != null) {
            try {
                Typeface typeface = Typeface.createFromAsset(context.getAssets(), "fonts/" + fontName);
                setTypeface(typeface);
            } catch (Exception e) {
                Log.e(TAG, "" + e);
            }
        }
        a.recycle();
    }
}
