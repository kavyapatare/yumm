package com.example.pratik.yumm;

/**
 * Created by kaveri on 29/8/17.
 */

public class FilterDetails implements FilterRowType {
    private int imageId;

    public FilterDetails(int imageId) {
        this.imageId = imageId;
    }

    public int getImageId() {
        return imageId;
    }

    public void setImageId(int imageId) {
        this.imageId = imageId;
    }

    @Override
    public int getItemType() {
        return FilterRowType.ITEM_TYPE;
    }
}
