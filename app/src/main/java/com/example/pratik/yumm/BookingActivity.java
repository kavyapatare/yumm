package com.example.pratik.yumm;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

/**
 * Created by pratik on 28/8/17.
 */
public class BookingActivity extends Activity {

    Spinner chooseDaySpinner,chooseTimeSpinner,personCountSpinner,whatToEatSpinner;
    Button btnReserve;
    String[] days=null;
    String[] time;
    String[] personCount;
    String[] orderNames;
    int cnt;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_booking);
        btnReserve=(Button)findViewById(R.id.btn_reserve);
        chooseDaySpinner=(Spinner)findViewById(R.id.chooseDay);
        personCountSpinner=(Spinner)findViewById(R.id.chooseGroupSize);
        chooseTimeSpinner=(Spinner)findViewById(R.id.chooseTime);
        whatToEatSpinner=(Spinner)findViewById(R.id.chooseWhatToEat);
        days=new String[7];

        Date today = new Date();
        Calendar c = Calendar.getInstance();
        c.setTime(today);
        days[0] = "Today";

        c.add(Calendar.DATE, 1);
        today = c.getTime();
        days[1] = "Tomorrow";

        for(cnt=2;cnt<7;cnt++) {
            c.add(Calendar.DATE, 1);
            today = c.getTime();
            days[cnt] = new SimpleDateFormat("dd/MM/yyyy").format(today);

        }


       ArrayAdapter<String> daysArrayAdapter = new ArrayAdapter<String>(this, R.layout.spinner_item, days);
        daysArrayAdapter.setDropDownViewResource(R.layout.spinner_item);
        chooseDaySpinner.setAdapter(daysArrayAdapter);
        chooseDaySpinner.setPrompt("PROMPT");

        String[] time=getResources().getStringArray(R.array.timeSpinner);
        ArrayAdapter timeArrayAdapter = new ArrayAdapter<String>(this, R.layout.spinner_item,R.id.txt_spinnerItem, time);
        chooseTimeSpinner.setAdapter(timeArrayAdapter);
        chooseTimeSpinner.setPrompt("PROMPT");

        String[] personCount=getResources().getStringArray(R.array.groupSizeSpinner);
        ArrayAdapter personsArrayAdapter = new ArrayAdapter<String>(this, R.layout.spinner_item,R.id.txt_spinnerItem, personCount);
        personCountSpinner.setAdapter(personsArrayAdapter);
        personCountSpinner.setPrompt("PROMPT");

        String[] orderNames=getResources().getStringArray(R.array.whatToEatSpinner);
        ArrayAdapter orderArrayAdapter = new ArrayAdapter<String>(this, R.layout.spinner_item,R.id.txt_spinnerItem, orderNames);
        whatToEatSpinner.setAdapter(orderArrayAdapter);
        whatToEatSpinner.setPrompt("PROMPT");

        btnReserve.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i=new Intent(BookingActivity.this,ReserveNowActivity.class);
                startActivity(i);
            }
        });

    }

}
