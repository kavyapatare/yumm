package com.example.pratik.yumm;

import android.app.Activity;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.View;
import android.widget.TextView;

/**
 * Created by kaveri on 29/8/17.
 */

public class ReserveNowActivity extends Activity {
    TextView addToCalendar,shareWithFrnds;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_reserve);
        addToCalendar=(TextView)findViewById(R.id.btn_addCalendarEvent);
        shareWithFrnds=(TextView)findViewById(R.id.btn_shareWithFriends);
        addToCalendar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

            }
        });
        shareWithFrnds.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

            }
        });

    }
}
