package com.example.pratik.yumm;

import android.content.Intent;
import android.media.Image;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {
    RecyclerView recyclerView;
    Button buttonSkip;
    RestaurantFilterAdapter adapter;
    List<FilterRowType> filterList;
    ImageView imageView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        imageView=(ImageView)findViewById(R.id.img_filterCategory);
        recyclerView=(RecyclerView)findViewById(R.id.recyclerView);


        filterList=new ArrayList<>();
        filterList.add(0,new FilterHeader("Neeraj"));
        filterList.add(1,new FilterDetails(R.drawable.image1));
        filterList.add(2,new FilterDetails(R.drawable.image2));
        filterList.add(3,new FilterDetails(R.drawable.image3));
        filterList.add(4,new FilterDetails(R.drawable.image4));
        filterList.add(5,new FilterDetails(R.drawable.image5));
        filterList.add(6,new FilterDetails(R.drawable.image6));


        adapter = new RestaurantFilterAdapter(filterList);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.setHasFixedSize(true);
        recyclerView.setAdapter(adapter);

        buttonSkip=(Button)findViewById(R.id.btnSkip);
        buttonSkip.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i=new Intent(MainActivity.this,MyBookingsActivity.class);
                startActivity(i);
            }
        });
    }
}
