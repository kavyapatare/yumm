package com.example.pratik.yumm;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

/**
 * Created by kaveri on 30/8/17.
 */

public class CustomSpinnerAdapter extends ArrayAdapter<String> {
    String[] spinnerValues;


    //Read more: http://mrbool.com/how-to-customize-spinner-in-android/28286#ixzz4rDtgRwGl


    public CustomSpinnerAdapter(Context ctx, int txtViewResourceId, String[] objects) {
        super(ctx, txtViewResourceId, objects);
        spinnerValues=objects;
    }

    @Override
    public View getDropDownView(int position, View cnvtView, ViewGroup prnt) {
        return getCustomView(position, cnvtView, prnt);
    }
    @Override
    public View getView(int pos, View cnvtView, ViewGroup prnt) {
        return getCustomView(pos, cnvtView, prnt);
    }

    public View getCustomView(int position, View convertView,
                              ViewGroup parent) {
        LayoutInflater inflater = LayoutInflater.from(getContext());
        View mySpinner = inflater.inflate(R.layout.spinner_item, parent,
                false);
        TextView main_text = (TextView) mySpinner
                .findViewById(R.id.txt_spinnerItem);
        main_text.setText(spinnerValues[position]);

        return mySpinner;
    }


}




